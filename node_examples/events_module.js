
const Logger = require('./logger')
const logger = new Logger();

//Using an instance of the custom class instead of an instance of eventemitter

//Register a listener
logger.on('messageLogged', (arg) => { //arg can be anything  //Can use arrow functions
    console.log('Listener Called', arg);
});

logger.log('message');




