//Variables in the function that wraps the entire module
// console.log(__filename);  
// console.log(__dirname);

// variable and function are scoped to this module onl
//Automatically private
var url = 'http://mylogger.io/log';

const EventEmitter = require('events');

//Better to extend eventemitter in a class when using it instead of acessing an actual event emitter object
class Logger extends EventEmitter {
    log(message) {
        //Send an HTTP request
        console.log(message);

        //Making a noise, produce - signalling that an event is happening
        this.emit('messageLogged', { id: 1, url: 'http://' }); 
    
    }
}


//Exporting certain member of this module: This is exporting it as an object
module.exports = Logger;

//Can also just export as a function
// module.exports = log;

