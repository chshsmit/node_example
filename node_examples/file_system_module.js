const fs = require('fs');

//Synchronous Form
// const files = fs.readdirSync('./');
// console.log(files);


//Asynchronous
//The second argument of asynchronous methods is a callback function that is called when the asynchronous action completes
fs.readdir('./', function(err, files) {
    if (err) console.log('Error', err);
    else console.log('Result', files);
});


//simulating an error
// fs.readdir('$', function(err, files) {
//     if (err) console.log('Error', err);
//     else console.log('Result', files);
// });

