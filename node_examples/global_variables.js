console.log()  //global object

setTimeout()  //use this to call a funcction after a delay
clearTimeout()

setInterval()
clearInterval()


//All of these are part of the window object and global variables
//global and window have entire scope


window.console.log() -> console.log()

var message = '';
window.message
global.console.log